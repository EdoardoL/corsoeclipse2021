package com.formula.transform;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.xml.transform.stream.StreamSource;

import org.xhtmlrenderer.pdf.ITextRenderer;


public class Any2PdfTransformer extends DocumentFormatTransformer{
  
  public Any2PdfTransformer() {
  }
  
  public boolean convertUsingHtml(File input, ByteArrayOutputStream output, String htmlXslName){

    if (input == null)
      return false;

    boolean ok;
    try {
      StreamSource ss = new StreamSource(input);
      ok = convertUsingHtml(ss, output, htmlXslName);
    } catch (Exception e) {
      ok = false;
      log.error(e);
      e.printStackTrace();
    }
    return ok;
  }


  public boolean convertUsingHtml(byte[] input, ByteArrayOutputStream output, String htmlXslName){

    if (input == null)
      return false;

    boolean ok;
    ByteArrayInputStream bais = null;
    try {
      bais = new ByteArrayInputStream(input);
      StreamSource ss = new StreamSource(bais);
      ok = convertUsingHtml(ss, output, htmlXslName);
    } catch (Exception e) {
      ok = false;
      log.error(e);
      e.printStackTrace();
    }finally{
      try{if (bais!=null) bais.close();} catch (IOException i) {}
    }
    return ok;
  }


  public boolean convertUsingHtml(StreamSource input, ByteArrayOutputStream output, String htmlXslName){

    if (input == null || output == null || htmlXslName == null)
      return false;

    boolean ok = true;
    //first convert input to html using given style sheet
    ByteArrayOutputStream htmlOS = new ByteArrayOutputStream();
    try {
      log.debug("Inizio trasformazione XML --> HTML");
      convert(input, htmlOS, htmlXslName);
      //log.info(htmlOS.toString());
      //then convert html to pdf
      log.debug("Inizio trasformazione HTML --> PDF");
      try {
        System.setProperty(AWT_HEADLESS_PROP, "true");
        if (log.isDebugEnabled()){
          log.debug("Impostata property " + AWT_HEADLESS_PROP + "=" + String.valueOf(System.getProperty(AWT_HEADLESS_PROP)));
        }
      } catch (Throwable e) {
        log.warn("Errore nell'impostazione di " + AWT_HEADLESS_PROP + "=true " + e.getMessage());
      }
      ITextRenderer renderer = new ITextRenderer();
      renderer.setDocumentFromString(htmlOS.toString(XSL_FILE_CHARSET));
      renderer.layout();
      renderer.createPDF(output);
      log.debug("Fine trasformazione in PDF");
    } catch (Exception e) {
      ok = false;
      log.error(e);
      e.printStackTrace();
    } finally {
      try {
        htmlOS.flush();
        htmlOS.close();
        htmlOS = null;
      } catch (IOException i) {}
    }
    return ok;
  }

/*
  public static void main(String[] args) {
    String xmlPath = "C:\\DcoreS\\SEPA\\test\\PEPPOL\\DDT_IN\\urn_notier_saospfe01uff_efatturapa_2016_PROVA_TR_120516_1_CP_DOCUMENTO_DI_TRASPORTO_01.xml";
    String xml2HtmlXsl = "xsl/DOCUMENTO_DI_TRASPORTO.xsl";
    File xmlFile = new File(xmlPath);

    String invPath = "C:\\DcoreS\\SEPA\\test\\PEPPOL\\FATTURE_IN\\urn_notier_SOGG-NOT-08982_2016_2016_U002F_ITE000000026_TD01_CP_FATTURA.xml";
    String inv2HtmlXsl = "xsl/fatturapa_v1.1.xsl";
    File invFile = new File(invPath);

    Any2PdfTransformer pdfTransformer = new Any2PdfTransformer();
    ByteArrayOutputStream pdfOS = new ByteArrayOutputStream();
    pdfTransformer.convertUsingHtml(xmlFile, pdfOS, xml2HtmlXsl);
    //pdfTransformer.convertUsingHtml(invFile, pdfOS, inv2HtmlXsl);

    try {
      String pdfFilePathName = "C:\\prova3.pdf"; //compilePdfFilePathName();
      File pdfDocument = pdfTransformer.writeXml(pdfOS, pdfFilePathName);
      pdfOS.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
*/
}
