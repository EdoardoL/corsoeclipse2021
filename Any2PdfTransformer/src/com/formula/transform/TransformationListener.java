package com.formula.transform;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.SourceLocator;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sf.saxon.s9api.MessageListener;
import net.sf.saxon.s9api.XdmNode;


public class TransformationListener implements MessageListener, ErrorListener {

  public static final String VALIDATION_OK = "OK";

  public static final Log log = LogFactory.getLog(TransformationListener.class);

  private boolean ok = true;

  public TransformationListener() {
  }

  public void warning(TransformerException e) throws TransformerException {
    log.debug(e);
  }

  public void error(TransformerException e) throws TransformerException {
    log.error(e);
    ok = false;
  }

  public void fatalError(TransformerException e) throws TransformerException {
    log.fatal(e);
    ok = false;
  }

  public void message(XdmNode content, boolean terminate, SourceLocator locator) {
    if (ObjectUtils.toString(content).trim().toUpperCase().equals(VALIDATION_OK)){
      log.info("Il documento XML ha superato le validazioni");
    }else{
      log.error("Il documento XML NON ha superato le validazioni");
      ok = false;
    }
  }

  public boolean isOk() {
    return ok;
  }

}
