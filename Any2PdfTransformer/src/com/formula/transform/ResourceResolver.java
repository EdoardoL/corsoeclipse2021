package com.formula.transform;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;


public class ResourceResolver implements URIResolver, LSResourceResolver {

  public static final Log log = LogFactory.getLog(ResourceResolver.class);

  private final String schemaBasePath;

  private Map<String, String> pathMap = new HashMap<String, String>();


  public ResourceResolver(String schemaBasePathName) {
    log.debug("ResourceResolver() schemaBasePathName = "+schemaBasePathName);
    schemaBasePath = StringUtils.removeStart(StringUtils.substringBeforeLast(schemaBasePathName,"/"),"/");
  }


  public Source resolve(String href, String base) throws TransformerException{
    try{
      String resource = schemaBasePath.concat("/").concat(href);
      log.debug("ResourceResolver.resolve() resource = "+resource);
//      ClassLoader loader = getClass().getClassLoader();
//      log.debug("ResourceResolver.resolve() getClass().getResource(resource) = "+getClass().getResource(resource));
//      log.debug("ResourceResolver.resolve() getClass().getClassLoader().getResource(resource) = "+getClass().getClassLoader().getResource(resource));
//      InputStream is = loader.getResourceAsStream(resource);
      InputStream is = getConfigFileIS(resource);
      return new StreamSource(is, resource);
    }
    catch(Exception ex){
      throw new TransformerException(ex);
    }
  }


  public LSInput resolveResource(String type, String namespaceURI,
      String publicId, String systemId, String baseURI) {
    // The base resource that includes this current resource
    String baseResourceName = null;
    String baseResourcePath = null;
    // Extract the current resource name
    String currentResourceName = systemId.substring(systemId.lastIndexOf("/") + 1);

    // If this resource hasn't been added yet
    try {
	    if (!pathMap.containsKey(currentResourceName)) {
	      if (baseURI != null) {
	        baseResourceName = baseURI.substring(baseURI.lastIndexOf("/") + 1);
	      }
	
	      // we dont need "./" since getResourceAsStream cannot understand it
	      if (systemId.startsWith("./")) {
	        systemId = systemId.substring(2, systemId.length());
	      }
	
	      // If the baseResourcePath has already been discovered, get that from pathMap
	      if (pathMap.containsKey(baseResourceName)) {
	        baseResourcePath = pathMap.get(baseResourceName);
	      } else {
	        // The baseResourcePath should be the schemaBasePath
	        baseResourcePath = schemaBasePath;
	      }
	
	      // Read the resource as input stream
	      String normalizedPath = getNormalizedPath(baseResourcePath, systemId);
	      log.debug("ResourceResolver.resolveResource() resource = "+normalizedPath);
//	      log.debug("ResourceResolver.resolveResource() getClass().getResource(resource) = "+getClass().getResource(normalizedPath));
//	      log.debug("ResourceResolver.resolveResource() getClass().getClassLoader().getResource(resource) = "+getClass().getClassLoader().getResource(normalizedPath));
//	      InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(normalizedPath);
	      InputStream resourceAsStream = getConfigFileIS(normalizedPath);
	      // if the current resource is not in the same path with base
	      // resource, add current resource's path to pathMap
	      if (systemId.contains("/")) {
	        pathMap.put(currentResourceName, normalizedPath.substring(0,normalizedPath.lastIndexOf("/")+1));
	      } else {
	        // The current resource should be at the same path as the base resource
	        pathMap.put(systemId, baseResourcePath);
	      }
	      return new LSInputImpl(publicId, systemId, resourceAsStream);
	    }
    } catch (IOException e) {
		log.error("Errore in resolveResurce type=" + type + " namespaceURI=" +namespaceURI + " publicId=" + publicId + " systemId=" + systemId + " baseURI=" + baseURI + " error=" + e.getMessage(), e);
	}
    return null;
  }


  private String getNormalizedPath(String basePath, String relativePath){
    if(!relativePath.startsWith("../")){
      if (!basePath.endsWith("/") && !relativePath.startsWith("/")){
        basePath = basePath.concat("/");
      }
      return basePath.concat(relativePath);
    }
    else{
      while(relativePath.startsWith("../")){
        basePath = StringUtils.substringBeforeLast(basePath, "/");
        relativePath = relativePath.substring(2);
      }
      return basePath+relativePath;
    }
  }


  public static InputStream getConfigFileIS(String configFilename) throws IOException {
	    log.trace("getConfigFileIS()");
	    InputStream configFileIS;
	    if (isExternal(configFilename)){
	      configFileIS = new FileInputStream(configFilename);
	    }else{
	      configFileIS = ResourceResolver.class.getClassLoader().getResource(configFilename).openStream();
	    }
	    if (log.isDebugEnabled()){
	      log.debug("getConfigFileIS(): configFilename=".concat(String.valueOf(configFilename))
	          .concat(" configFilePath=").concat(String.valueOf(configFilename))
	          .concat(" configFileIS=").concat(String.valueOf(configFileIS)));
	    }
	    return configFileIS;
	  }


  private static boolean isExternal(String resourcePath) {
    try {
      boolean unloaded = ResourceResolver.class.getClassLoader().getResource(resourcePath)==null;
      log.debug("Is ".concat(resourcePath).concat(" external? ").concat(String.valueOf(unloaded)));
      return unloaded;
    } catch (Exception e) {
      log.debug(resourcePath.concat(" is external"));
      return true;
    }
  }
}