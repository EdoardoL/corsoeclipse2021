package com.formula.transform;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.s9api.XsltTransformer;


public class DocumentFormatTransformer {

  public static final String XSL_FILE_CHARSET = "UTF-8";
  public static final String AWT_HEADLESS_PROP = "java.awt.headless";

  static {
    //System.setProperty("java.protocol.handler.pkgs", "com.formula.dxf");
  }

  public static final Log log = LogFactory.getLog(DocumentFormatTransformer.class);

  protected Processor proc;
  
  public DocumentFormatTransformer() {
    proc = new Processor(false);
  }

  public void convert(Source source, ByteArrayOutputStream output, String xslName)
      throws Exception {
    XsltTransformer trans = buildTransformer(xslName);
    XdmNode sourceNode = proc.newDocumentBuilder().build(source);
    Serializer out = proc.newSerializer();
    out.setOutputProperty(Serializer.Property.INDENT, "yes");
    out.setOutputProperty(Serializer.Property.METHOD, "xml");
    out.setOutputStream(output);
    trans.setInitialContextNode(sourceNode);
    trans.setDestination(out);
    trans.setURIResolver(new ResourceResolver(xslName));
    trans.transform();
    out.close();
  }

  protected XsltTransformer buildTransformer(String stylesheetName) throws SaxonApiException, IOException {
    InputStream stylesheetIS = ResourceResolver.getConfigFileIS(stylesheetName);
    Reader stylesheetReader = new InputStreamReader(stylesheetIS, Charset.forName(XSL_FILE_CHARSET));
    XsltCompiler comp = proc.newXsltCompiler();
    TransformationListener listener = new TransformationListener();
    comp.setErrorListener(listener);
    comp.setURIResolver(new ResourceResolver(stylesheetName));
    XsltExecutable exp = comp.compile(new StreamSource(stylesheetReader));
    XsltTransformer trans = exp.load();
    return trans;
  }

  public File writeXml(ByteArrayOutputStream xmlOS, String filename, String outDir) throws IOException{
    String outputFilePathName = outDir.endsWith(File.separator)?outDir+filename:outDir+File.separator+filename;
    return writeXml(xmlOS, outputFilePathName);
  }

  public File writeXml(ByteArrayOutputStream xmlOS, String outputFilePathName) throws IOException{
    File outputFile = new File(outputFilePathName);
    File parent = outputFile.getParentFile();
    if (parent != null && !parent.exists() && !parent.mkdirs()){
      throw new IOException("Impossibile creare il file " + outputFilePathName);
    }else{
      FileOutputStream outputFileOS = null;
      try {
        outputFileOS = new FileOutputStream(outputFile);
        xmlOS.writeTo(outputFileOS);
        log.info("Creato il file " + outputFile);
      } finally {
        if (outputFileOS != null){
          outputFileOS.flush();
          outputFileOS.close();
          outputFileOS = null;
        }
      }
      return outputFile;      
    }
  }

}
